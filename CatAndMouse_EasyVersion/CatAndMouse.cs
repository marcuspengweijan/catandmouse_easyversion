﻿using System;
using System.Text.RegularExpressions;

namespace CatAndMouse_EasyVersion
{
    public class CatAndMouse
    {
        private readonly string _input;
        private const int CatCanMove = 3;
        private CatAction CatAction => Regex.IsMatch(_input, $"C.{{0,{CatCanMove}}}m") 
            ? CatAction.Caught : CatAction.Escaped;
        
        public CatAndMouse(string input)
        {
            _input = input;
            ValidateInput();
        }
        
        public string Go()
        {
            return CatAction.ToStringWithExclamation();
        }

        private void ValidateInput()
        {
            if (!Regex.IsMatch(_input, "C.{0,}m"))
            {
                throw new InvalidOperationException();
            }
        }
    }
}