﻿namespace CatAndMouse_EasyVersion
{
    public enum CatAction { Caught, Escaped };
    
    public static class CatActionExtension
    {
        public static string ToStringWithExclamation(this CatAction catAction) => $"{catAction.ToString()}!";
    }
}