﻿using System;
using CatAndMouse_EasyVersion;
using NUnit.Framework;

namespace CatAndMouse_EasyVersionTests
{
    [TestFixture]
    public class CatAndMouseTests
    {
        private CatAndMouse _target;
        
        [Test]
        public void CatAndMouseTest_Escaped()
        {
            GivenInput("C....m");
            ShouldResponse("Escaped!");
        }
        
        [Test]
        public void CatAndMouseTest_Caught()
        {
            GivenInput("C..m");
            ShouldResponse("Caught!");
        }
        
        [Test]
        public void CatAndMouseTest_InvalidOperation()
        {
            Assert.Throws<InvalidOperationException>(() => {
                GivenInput("abc");
            });
        }

        private void GivenInput(string input)
        {
            _target = new CatAndMouse(input);
        }
        
        private void ShouldResponse(string expected)
        {
            Assert.AreEqual(expected, _target.Go());
        }
    }
}